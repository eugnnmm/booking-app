import React from 'react';
import { Modal, Button } from 'flowbite-react';


const BookingConfirm = ({ isOpen, onClose, selectedDates, selectedTransfer }) => {
  if (!isOpen) {
    return null;
  }

  return (
    <Modal
      show={isOpen}
      onClose={onClose}
    >
      <Modal.Header>
        Booking Confirmation
      </Modal.Header>
      <Modal.Body>
        <div className="space-y-6">
          <p>
            Your transfer booking for <strong>{selectedTransfer.description}</strong> has been confirmed.
          </p>
          <p>Date Range: {selectedDates[0].toDateString()} - {selectedDates[1].toDateString()}</p>
          <p>Thank you for your booking!</p>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={onClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>

  )
}

BookingConfirm.displayName = 'BookingConfirm';

export default BookingConfirm;
