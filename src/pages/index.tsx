import { Inter } from 'next/font/google'
import { Card } from 'flowbite-react'
import { useState } from 'react'
import BookingConfirm from '../BookingConfirm'
import Picker from '../Picker'

const inter = Inter({ subsets: ['latin'] });

const transferOptions = [
  {
    id: 1,
    description: 'Luxury Transfer - Private Car',
  },
  {
    id: 2,
    description: 'Economy Transfer - Shared Shuttle',
  },
  {
    id: 3,
    description: 'Family Transfer - Minivan',
  },
  {
    id: 4,
    description: 'Group Transfer - Bus',
  },
];

const Home = () => {
  const [selectedTransfer, setSelectedTransfer] = useState(null);
  const [showConfirmationModal, setShowConfirmationModal] = useState(false);
  const [selectedDates, setSelecteDates] = useState(null)

  const handleSelectTransfer = (transfer) => {
    setSelectedTransfer(transfer);
  };

  const handleBook = () => {
    console.log(selectedDates)
    if (!selectedTransfer) {
      alert('Please select a transfer.');
      return;
    }

    if (!selectedDates) {
      alert('Please select the date range option.');
      return;
    }

    setShowConfirmationModal(true);
  };

  const onDateSelect = (selectedDates) => {
    setSelecteDates(selectedDates)
  }

  return (
    <main className={`flex min-h-screen flex-col items-center justify-between p-24 ${inter.className}`}>
      <div className="w-full max-w-2xl">
        <Picker onDateRangeChange={onDateSelect} />
        <div className="mt-8 grid grid-cols-1 gap-6">
          {transferOptions.map((transfer) => (
            <Card
              key={transfer.id}
              onClick={() => handleSelectTransfer(transfer)}
              className={`cursor-pointer ${selectedTransfer?.id === transfer.id ? 'ring-2 ring-blue-500' : ''}`}
            >
              <div className="font-normal text-slate-600 text-lg">{transfer.description}</div>

            </Card>
          ))}
        </div>
        <button
          onClick={handleBook}
          className="bg-blue-500 hover:bg-blue-600 text-white py-4 px-4 rounded text-lg mt-4 w-full focus:outline-none focus:ring-4 focus:ring-blue-300"
        >
          Book
        </button>
      </div>
      <BookingConfirm
        isOpen={showConfirmationModal}
        onClose={() => setShowConfirmationModal(false)}
        selectedTransfer={selectedTransfer}
        selectedDates={selectedDates}
      />
    </main>
  );
};

export default Home;
